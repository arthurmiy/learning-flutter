//this package contains the necessary elements to build the ui, there are some alternatives like cupertino (that targets IOS app development)
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:teaching_flutter/main.dart';
import 'package:teaching_flutter/pages/configuration.dart';
import 'package:teaching_flutter/pages/conversion_page.dart';
import 'package:teaching_flutter/widgets/MyCustomCard.dart';
import 'package:teaching_flutter/widgets/my_custom_button.dart';

//to create a statefulWidget automatically you can type stful and press enter
class IMCCalculator extends StatefulWidget {
  //static variables can be seen without instantiation
  //it's a good practice to keep the name of the route as a static parameter of the class to be created
  static final String route = '/';

  //Create a new state
  @override
  _IMCCalculatorState createState() => _IMCCalculatorState();
}

class _IMCCalculatorState extends State<IMCCalculator> {
  //variables declaration
  TextEditingController weight;
  String weightErrorMsg;

  TextEditingController height;
  String heightErrorMsg;

  double result;

  @override
  void initState() {
    super.initState();
    //this function is executed when the state is initialized, hot reloads or setState events doesn't trigger this function
    //in this function you can initialize some parameters, or execute some code before the page is firstly constructed
    height = TextEditingController();
    weight = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    //this function is called frequently (basically, every change in the UI is only visible after a build)
    //by stateful widgets this function is called by the set state function
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              'assets/imagem.png',
              height: 50,
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                'Calculadora de IMC',
              ),
            ),
          ],
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () async {
                //the async modifier indicates that the function is asynchronous
                /*
                    The Navigator class can be used to push and/or pop flutter pages
                    Int this project we are using only named routes

                    by named routes only the name is needed, the instructions for the page build are given
                    by the map on the material app widget (at the main.dart file)
                 */

                //the await modifier can be used only in async or async* functions
                //it makes the program to wait the results of the called function
                bool tmp = isMetric;
                await Navigator.pushNamed(context, SettingsPage.route);
                if (tmp != isMetric) _clear();
                setState(() {});

                //async functions return Futures i.e. results that will come in the future (async)
                //to convert  a Future<type> to a type you have to use the await modifier or  use the function .then(callback)
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MyCustomCard(
                  //custom widget
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'Dados',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: TextField(
                              controller: weight,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  errorText: weightErrorMsg,
                                  hintText: 'Digite seu peso',
                                  labelText: 'Peso',
                                  suffix: isMetric ? Text('kg') : Text('lbs')),
                            ),
                          ),
                          IconButton(
                              tooltip: 'Converter peso',
                              icon: Icon(Icons.track_changes),
                              onPressed: () => _convert(false))
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: height,
                              decoration: InputDecoration(
                                  errorText: heightErrorMsg,
                                  hintText: 'Digite sua altura',
                                  labelText: 'Altura',
                                  suffix: isMetric ? Text('m') : Text('ft')),
                            ),
                          ),
                          IconButton(
                            tooltip: 'Converter altura',
                            icon: Icon(Icons.track_changes),
                            onPressed: () => _convert(true),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      MyCustomButton('Calcular meu IMC',
                          onPress: _calculateIMC),
                      SizedBox(
                        height: 40,
                      ),
                      MyCustomButton(
                        'Limpar Campos',
                        onPress: _clear,
                        icon: Icons.remove_circle,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                AnimatedContainer(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    //You can put conditionals in a single line as following
                    //variable = condition ? ifTrueValue : ifFalseValue;
                    color: result == null
                        ? Colors.transparent
                        : result < 17 || result > 25
                            ? Theme.of(context).errorColor
                            : Colors.green,
                  ),
                  duration: Duration(milliseconds: 500),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Resultado: ${result == null ? '-' : result.round()}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*
      When the name of a function begins with underline it means that this function is private (is not visible from outside the class)
   */
  void _calculateIMC() {
    //reset previous values
    weightErrorMsg = null;
    heightErrorMsg = null;
    result = null;
    setState(() {});

    //sets error msg
    if (weight.text.isEmpty) weightErrorMsg = 'Peso é obrigatório';
    if (height.text.isEmpty) heightErrorMsg = 'Altura é obrigatória';

    if (weight.text.isNotEmpty && height.text.isNotEmpty) {
      //if not empty
      //verify weight
      double weightTmp = double.tryParse(weight.text);
      if (weightTmp == null) {
        setState(() {
          weightErrorMsg = 'Valor inválido de peso';
        });

        return;
      }

      //verify weight
      double heightTmp = double.tryParse(height.text);
      if (heightTmp == null) {
        setState(() {
          heightErrorMsg = 'Valor inválido de altura';
        });
        return;
      }

      //conversion
      weightTmp = isMetric ? weightTmp : weightTmp / 2.2;
      heightTmp = isMetric ? heightTmp : heightTmp / 3.28;

      result = weightTmp / pow(heightTmp, 2);
    }

    //rebuild to reflect changes in the interface
    setState(() {});
  }

  Future<void> _convert(bool isHeight) async {
/*
                                  -other pages can be called to get some result

                                  -it can be done in a async function using the Navigator and the await modifier as follows

                               var value = await Navigator.pushNamed(
                                  context, route) as double;

                                   -more than just receive results, you can also pass parameters to other pages

                                   -in the case of named routs there's a cookbook in the
                                   flutter docs that explains how to proceed to properly pass the parameters
                                      -https://flutter.dev/docs/cookbook/navigation/navigate-with-arguments

                                    -In other to get the results the called page should return the values in the Navigator.pop function:
                                    Navigator.pop(context,result);


                               */
    var value = await Navigator.pushNamed(context, ConversionPage.route,
        arguments: isHeight) as double;

    if (value != null) if (!isHeight)
      weight.text = '${value.toStringAsFixed(2)}';
    if (isHeight) height.text = '${value.toStringAsFixed(2)}';

    setState(() {});
  }

  void _clear() {
    setState(() {
      height.text = '';
      weight.text = '';
      result = null;
    });
  }
}

/*
    Relevant Widgets:
      -SafeArea - ensure that the app area will be visible without interference from the native system elements (such as status bar)
      -Scaffold: Is the base of an app page. It's recommended to have this element as the Base of the page to pass theme parameters to its children.
                 it can or not contain
                      -AppBar
                      -Body
                      -BottomNavigationBar
                      -FloatingActionButton
      -Container:
      -Buttons:
        -RaisedButton
        -OutlineButton
        -IconButton
      -SizedBox
      -Padding
      -SingleChildScrollView
      -Text
      -TextField
 */

/*
    Useful Shortcuts

    - Alt+Enter : Shows available options to import libraries
    - Ctrl+Enter : Shows the autocomplete pop-up with input suggestions
    - Scroll click : Goes to the declaration/use of variables, classes, etc.

    -MULTIPLE LINE SELECTION:
      - ctrl+alt+shift+click : New insertion point will be added or removed by click
      - alt+j : when pressed with text content selected, will select the next match
      - scroll click and drag : will select multiple lines in sequence


    shift+alt+arrow up/down: moves selection or line up and down in the code

 */
