import 'package:flutter/material.dart';
import 'package:teaching_flutter/widgets/MyCustomCard.dart';
import 'package:teaching_flutter/widgets/my_custom_button.dart';

import '../main.dart';

class ConversionPage extends StatefulWidget {
  static final String route = '/conv';
  @override
  _ConversionPageState createState() => _ConversionPageState();
}

class _ConversionPageState extends State<ConversionPage> {
  TextEditingController text;
  String errorMsg;

  @override
  void initState() {
    super.initState();
    text = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    //similar to the main page
    final bool isHeight = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Conversor',
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MyCustomCard(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Valor a ser convertido',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      TextField(
                        controller: text,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          errorText: errorMsg,
                          hintText: 'Digite o valor que deseja converter',
                          labelText: 'Valor',
                          suffix: Text(_getUnit(isHeight)),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyCustomButton('Converter', onPress: () {
                        double tmp = _convertValue(isHeight);
                        if (tmp != null) {
                          /*
                                  -other pages can be called to get some result

                                  -it can be done in a async function using the Navigator and the await modifier as follows

                               var value = await Navigator.pushNamed(
                                  context, route) as double;

                                   -more than just receive results, you can also pass parameters to other pages

                                   -in the case of named routs there's a cookbook in the
                                   flutter docs that explains how to proceed to properly pass the parameters
                                      -https://flutter.dev/docs/cookbook/navigation/navigate-with-arguments

                                    -In other to get the results the called page should return the values in the Navigator.pop function:
                                    Navigator.pop(context,result);


                               */
                          Navigator.pop(context, tmp);
                        } else {
                          setState(() {
                            errorMsg = 'Digite um valor válido';
                          });
                        }
                      })
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _getUnit(bool isHeight) {
    String returnValue = '';
    if (isHeight) {
      returnValue = isMetric ? 'ft' : 'm';
    } else {
      returnValue = isMetric ? 'lbs' : 'kg';
    }
    return returnValue;
  }

  double _convertValue(bool isHeight) {
    errorMsg = null;
    double returnValue;
    double typedValue = double.tryParse(text.text);
    if (typedValue == null) {
      errorMsg = 'Valor inválido';
      return null;
    }
    if (isHeight) {
      returnValue = isMetric ? typedValue / 3.28 : typedValue * 3.28;
    } else {
      returnValue = isMetric ? typedValue / 2.2 : typedValue * 2.2;
    }
    return returnValue;
  }
}
