import 'package:flutter/material.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:teaching_flutter/widgets/MyCustomCard.dart';

import '../main.dart';

/*
      Name convention:
        _name: private variable
        lower camelcase: functions and variables
        upper camelcase: Classes
 */
class SettingsPage extends StatefulWidget {
  static final String route = '/Settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configurações'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            children: [
              MyCustomCard(
                child: Row(
                  children: [
                    Checkbox(
                        value: isMetric,
                        onChanged: (v) {
                          setState(() {
                            isMetric = v;
                            //save setting used the imported package
                            Settings().save('isMetric', isMetric);
                          });
                        }),
                    SizedBox(
                      width: 10,
                    ),
                    Text('Sistema métrico')
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
