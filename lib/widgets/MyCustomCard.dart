import 'package:flutter/material.dart';

class MyCustomCard extends StatelessWidget {
  final Widget child;

  MyCustomCard({this.child});
  @override
  Widget build(BuildContext context) {
    //PhysicalModel: adds shadow to the child widget by setting an elevation
    return PhysicalModel(
      elevation: 20,
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(20),
      child: Container(
        constraints: BoxConstraints(maxWidth: 500),
        decoration: BoxDecoration(
          color: Theme.of(context)
              .primaryColorLight, //retrieves the theme from context
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: child ??
              Container(), // double question marks allows to avoid nulls (if the first element is null the element after the question marks is returned)
        ),
      ),
    );
  }
}
