import 'package:flutter/material.dart';

//the shortcut to create a stateless widget is stless
class MyCustomButton extends StatelessWidget {
  //Stateless widgets are simple, they don't store any state, all internal variables are final because they are
  // not meant to change during the life cycle of the widget

  //to a button for example you may have the following inputs:
  //  -Text to be displayed
  //  -Icon
  //  -on press callback
  final String text;
  final IconData icon;
  final GestureTapCallback onPress;

  //if you want to change the text inside o stateless widget you basically destroy it and make a new one with the
  // desired value (passed as parameter)

  //Constructor:
  //the constructor gets the inputs from the parent widget.

  //The easiest way to declare the arguments inside the constructor is by typing this.variableName

  //you can declare a parameter to be named when you put them inside braces:
  /*
      Example:
          Declaration of constructor : MyClass (this.mandatoryNotNamedParameter, {@required this.requiredNamedParameter, this.optionalNamedParameter, this.optionalNamedParameterWithDefaultValue='defaultStringValue'});
          Constructor calling 1 : MyClass(mandatoryParameterValue, requiredNamedParameter: requiredNamedParameterValue);
          Constructor calling 1 : MyClass(mandatoryParameterValue, requiredNamedParameter: requiredNamedParameterValue, optionalNamedParameter: optionalNamedParameterValue);
          Constructor calling 1 : MyClass(mandatoryParameterValue, requiredNamedParameter: requiredNamedParameterValue);
   */
  MyCustomButton(this.text,
      {this.icon = Icons.calculate_outlined, @required this.onPress});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        side: BorderSide(width: 2),
      ),
      onPressed: onPress,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(icon),
            SizedBox(
              width: 8,
            ),
            Text(text)
          ],
        ),
      ),
    );
  }
}

/*
  Tips:
    -When you click over some widget or other component the android studio will show some tips/tools to make common changes in the code (a lamp at the left side)
    -A useful function is the wrap options to add the selected element to a Container, Row, Padding, etc.
    -It change convert also a stateless widget to a stateful with no effort
    -The lamp will be red if the tips are related to error correction. In this case it can help to import a library, implement a method, etc
 */
