import 'package:flutter/material.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:teaching_flutter/pages/configuration.dart';
import 'package:teaching_flutter/pages/conversion_page.dart';
import 'package:teaching_flutter/pages/imc_calculator.dart';

bool isMetric = true;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  isMetric = await Settings().getBool('isMetric', true);
  //first function called at the beginning of execution
  runApp(MyApp());
}

//StatelessWidget: Doesn't store any state, just shows something in the screen. Is rebuilt constantly (runs the build function)
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IMC Calculator',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //the initial route is used to indicate the first page to be open in a NamedRout Navigation approach
      initialRoute: '/',
      //the routes input is a simple map that indicates how to construct the page depending on the name passed to the navigator
      routes: {
        //It's a good practice to store the path as static parameter of the class
        IMCCalculator.route: (context) =>
            IMCCalculator(), //'route': (context) => Page()
        SettingsPage.route: (context) => SettingsPage(),
        ConversionPage.route: (context) => ConversionPage(),
      },
    );
  }
}
