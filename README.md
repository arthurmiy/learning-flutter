# Calculadora de IMC

Aplicativo simples para fins educativos.

Esse aplicativo calcula o IMC de acordo com os dados colocados. O resultado é destacado em vermelho ou verde dependendo do valor do IMC (sendo verde dentro da faixa esperada).

Para aumentar a complexidade do app ele oferece a opção de conversão de unidades e uma página de configuração que permite selecionar o sistema de medidas utilizado.

Uma versão online pode ser encontrada [aqui](https://imc-calculate.web.app/#/)

# Tópicos abordados

Nesse exemplo alguns tópicos interessantes para o desenvolvimento de apps em flutter são abordados. Nos próximos tópicos será apresentado um pouco mais sobre os diferentes temas abordados.

## Stateless x Stateful widgets  e função setState

No projeto foram utilizados os dois tipos de widget e uma breve explicação é fornecida.

## Navegação utilizando named routes

O projeto utiliza 'named routes' para navegar entre as páginas do app e exemplifica como utilizar mantendo boas práticas de programação

## Passagem de parâmetros entre telas (envio e recebimento)

Ainda utilizando as rotas nomeadas a função de passagem de parâmetros é utilizada na tela de conversão que recebe o tipo de dado a ser convertido e retorna o valor calculado

## Customização simples de widgets

Alguns widgets customizados foram utilizados (e.g. botão) e a partir deles é possível entender como organizar melhor o código por separação em widgets e como declarar os parâmetros para que sejam parâmetros nomeados, obrigatórios ou que tenham valores padrão pré-estabelecidos

## Widgets básicos

Conhecer widgets disponíveis faz parte de saber bem utilizar o flutter para a criação de apps. Alguns dos widgets mais básicos foram utilizados para a criação desse app e poderá servir de introdução para a enorme gama disponível.

## Atalhos interessantes do Android Studio

É interessante saber atalhos que facilitam a vida na hora de programar. Alguns desses atalhos, que permitem, desde encapsular widgets dentro de uma coluna ou container até mesmo escrever em várias linhas ao mesmo tempo, foram inclusos em forma de comentário ao longo do código 

## Inserção de assets no projeto (Imagem)

Para inserir imagens e outros recursos é importante alterar devidamente o arquivo pubspec.yaml, sendo assim uma imagem foi incluída apenas para demonstrar como fazer

## Padrões de nomeação

Foi indicado também como as classes, métodos, variáveis e afins são nomeados

## elementos de tipo static, final, private, atribuições condicionais de uma linha (operador '?') e teste de elementos nulos (operador '??') 

Algumas particularidades da linguagem dart também foram explicadas em forma de comentário

## Funções assíncronas e básico sobre Futures

Uma introdução básica sobre funções assíncronas foi dada e, por consequencia disso, também foi falado sobre os Futures. Não foi abordado, entretanto, os Streams e async*
